import { Timetable } from '../models/timetable';

export interface TimetableRefreshRequestedAction {
    type: "timetable-refresh-requested"
}

export function timetableRefreshRequestedAction(): TimetableRefreshRequestedAction {
    return {
        type: "timetable-refresh-requested"
    };
}

export interface TimetableUpdateSucceededAction {
    type: "timetable-update-succeeded",
    data: Timetable
}

export function timetableUpdateSucceededAction(data: Timetable): TimetableUpdateSucceededAction {
    return {
        type: "timetable-update-succeeded",
        data
    };
}

export interface TimetableUpdateFailedAction {
    type: "timetable-update-failed",
    error: string
}

export function timetableUpdateFailedAction(error: string): TimetableUpdateFailedAction {
    return {
        type: "timetable-update-failed",
        error
    };
}

export interface TimetableSwitchStationAction {
    type: "timetable-switch-station",
    newStation: number
}

export function timetableSwitchStationAction(newStation: number): TimetableSwitchStationAction {
    return {
        type: "timetable-switch-station",
        newStation
    }
}

export type TimetableAction = TimetableRefreshRequestedAction
                            | TimetableUpdateSucceededAction
                            | TimetableUpdateFailedAction
                            | TimetableSwitchStationAction;