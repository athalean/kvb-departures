import { TimetableAction } from './timetable'

export type AppAction = TimetableAction;