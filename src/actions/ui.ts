

export interface UIDepartureStationToggleMenuAction {
    type: "ui-departure-station-toggle-menu"
}

export function uiDepartureStationToggleMenuAction(): UIDepartureStationToggleMenuAction {
    return {
        type: "ui-departure-station-toggle-menu"
    }
}

export interface UIDepartureStationHideMenuAction {
    type: "ui-departure-station-hide-menu"
}

export function uiDepartureStationHideMenuAction(): UIDepartureStationHideMenuAction {
    return {
        type: "ui-departure-station-hide-menu"
    }
}


export type UIAction = UIDepartureStationToggleMenuAction
                     | UIDepartureStationHideMenuAction;