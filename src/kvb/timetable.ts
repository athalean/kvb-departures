import { Timetable, TimetableDeparture } from '../models/timetable';
import * as cheerio from 'cheerio';

export type StationID = number;

function charsetCorrectingFetch(url: string): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        let xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);
        xhr.overrideMimeType('text/html;charset=iso-8859-1');
        xhr.onreadystatechange = function() {
            if(this.readyState === 4) {
                if(this.status >= 200 && this.status < 400)
                    resolve(this.responseText);
                else
                    reject(this.statusText);
            }
        }
        xhr.send();
    });
}

export function fetchDepartures(station: StationID): Promise<Timetable> {
    return charsetCorrectingFetch(
            `http://www.kvb-koeln.de/generated/?aktion=show&code=${encodeURIComponent(station.toString())}&title=graph`
        )
        .then((site): Timetable => {
            const $ = cheerio.load(site);

            const departures = $('table').eq(1).find('tr')
                .filter((i) => i > 0)
                .map(function(this: Cheerio): TimetableDeparture {
                    const el = $(this).children('td');
                    return {
                        line: el.eq(0).text().trim(),
                        destination: el.eq(1).text().trim(),
                        departure: el.eq(2).text().trim().toLowerCase() === "sofort" ? "now" : parseInt(el.eq(2).text().trim())
                    } 
            }).get() as any as TimetableDeparture[];
            
            return {
                stationID: station,
                stationName: $('.top_head_rot_small').text(),
                departures
            } as Timetable;
        });
}