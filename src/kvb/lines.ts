export const enum VehicleType {
    Bus,
    Stadtbahn,
    SBahn,
    DB,
    Taxi,
    Unknown
}

export const guessVehicleTypeFromLine = (line: string): VehicleType => {
    let effectiveLine = line;
    if(line.startsWith("S"))
        return VehicleType.SBahn;
    if(line.startsWith("U"))
        return VehicleType.Stadtbahn;
    if(line.startsWith("RE") 
        || line.startsWith("RB") 
        || line.startsWith("RRX"))
            return VehicleType.DB;
    if(line.startsWith("AST")
        || line.indexOf("Taxi") > -1)
            return VehicleType.Taxi;
    
    if(line.startsWith("E")) {
        if(line === "E")
            return VehicleType.Stadtbahn;
        effectiveLine = line.substr(1);
    }
       
    const lineNumber = parseInt(effectiveLine);

    // everything that starts with none of the text snippets
    // covered above and isn't a number is considered unknown
    if(lineNumber === NaN)
        return VehicleType.Unknown;

    // from here on it's only numbers
    // three-digits and higher are busses
    if(lineNumber >= 100)
        return VehicleType.Bus;
    
    // two-digits and lower are stadtbahns
    return VehicleType.Stadtbahn;
}

export const guessColourForLine = (line: string) => {

    switch(line) {
        case "S6":
            return "#FB3099";
        case "S11":
            return "#E6792D";
        case "S12":
            return "#00AA4F";
        case "S13":
        case "S19":
            return "#00B8F1";
        case "RB25":
            return "#BB8FC7";


    }

    const lineNumber = parseInt(line);

    switch(lineNumber) {
        case 1:
            return "#E0001D";
        case 3: 
            return "#F39FC1";
        case 4:
            return "#E76EA1";
        case 5:
            return "#A8A0D3";
        case 7:
            return "#EB9363";
        case 9:
            return "#ED9589";
        case 12:
            return "#92B803";
        case 13:
            return "#A88460";
        case 15:
            return "#46AD20";
        case 16:
            return "#1EB7B4";
        case 17:
            return "#7ECFEC";
        case 18:
            return "#0390C6";      
    }

    return null;
}