import * as React from 'react';

interface LinkProps {
    children?: (string | JSX.Element)[];
    color?: string;
    onClick: () => void;
}

export const Link = ({children, color, onClick}: LinkProps) => {
    let style = {    };
    if(color)
        (style as any).color = color;
    return <a onClick={onClick} className="ui-link" style={style}>
        <div>{children}</div>
    </a>
}