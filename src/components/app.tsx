import * as React from 'react';
import * as PropTypes from 'prop-types';

import { createStore, Store } from 'redux';
import { Provider, connect } from 'react-redux';
import reducer from '../reducers/app';

import { AppState, TimetableState } from '../reducers/app';
import { timetableRefreshRequestedAction } from '../actions/timetable';
import Timetable from './timetable';

import { Button, Table } from 'semantic-ui-react'


export const store = createStore(
    reducer
) as Store<AppState>;

interface AppProps {
    timetable: TimetableState;
    selectedStation: number;
    update: () => void;
}

class AppComponent extends React.Component<AppProps, {}> {
    refreshTimetable() {
        store.dispatch(timetableRefreshRequestedAction())
    }
    componentDidMount() {
        setInterval(() => this.refreshTimetable(), 15*1000);
        this.refreshTimetable();
    }
    render() {
        return <div>
            <Timetable data={this.props.timetable!.data} />
        </div>
    }
}

const AppContainer = 
connect((state: AppState) => {
    return {
        timetable: state.timetable,
        selectedStation: state.timetable.selectedStation
    }
},
(dispatch) => {
    return {
        update: (station: number) => {
            dispatch(timetableRefreshRequestedAction())    
        }
    }
})
((props: AppProps) => {
    return <AppComponent {...props} />
})

interface RootProps {
    initialStation: number;
}

export const Root = ({initialStation}: RootProps) => {
    return <Provider store={store}>
        <AppContainer />
    </Provider>
}

