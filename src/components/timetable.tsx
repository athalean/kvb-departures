import * as React from 'react';
import { Timetable, TimetableDeparture } from '../models/timetable';

import { Maybe } from 'tsmonad';
import { Table, TableRow, TableCell, TableBody, Icon, Header, Segment, Label, Image, Dropdown, Grid } from 'semantic-ui-react';
import { guessVehicleTypeFromLine, guessColourForLine, VehicleType } from '../kvb/lines';
import { Link } from './link';

import {
    uiDepartureStationToggleMenuAction,
    uiDepartureStationHideMenuAction
} from '../actions/ui';

import {
    timetableSwitchStationAction,
    timetableRefreshRequestedAction
} from '../actions/timetable';

import { AppState } from '../reducers/app';
import { StationEntry } from '../reducers/timetable'

import { connect } from 'react-redux';

interface VehicleIconProps {
    type: VehicleType
}

const VehicleIcon = ({type}: VehicleIconProps) => {
    switch(type) {
        case VehicleType.Bus:
            return <Icon name="bus" />;
        case VehicleType.Stadtbahn:
            return <Icon name="subway" />;
        case VehicleType.SBahn:
            return <Image src="img/sbahn.svg" height={20} style={{padding: "0 .1em"}} inline />;
        case VehicleType.DB:
            return <Icon name="train" />;
        case VehicleType.Taxi:
            return <Icon name="taxi" />;
        case VehicleType.Unknown:
            return <div/>;
    }
}

interface LineLabelProps {
    line: string
}

const LineLabel = ({line}: LineLabelProps) => {
    const colour = guessColourForLine(line);
    return <div>
        {
            colour === null 
            ? <Label circular size="large">{line}</Label>
            : <Label circular size="large" style={{backgroundColor: colour, color: "#eeeeee"}}>{line}</Label>
        }
    </div>
}

interface TimetableRowProps {
    row: TimetableDeparture
}

const TimetableRow = ({row}: TimetableRowProps) => {
    return <TableRow positive={row.departure === "now"} style={{ fontSize: "1.2em" }}>
        <TableCell width={1} textAlign="center"><VehicleIcon type={guessVehicleTypeFromLine(row.line)} /></TableCell>
        <TableCell width={2} textAlign="left"><LineLabel line={row.line} /></TableCell>

        <TableCell width={8} style={{fontWeight: "bold"}}>{row.destination}</TableCell>
        <TableCell width={4} textAlign="right">{row.departure === "now" 
            ? <div><Icon name="arrow circle outline right" /> Sofort</div>
            : <div><Icon name="clock" /> {row.departure} min</div>}
        </TableCell>
    </TableRow>
}

interface KVBHeaderProps {
    title: string,
    onHeaderClick?: () => void,
    onSelectStation: (code: number) => void,
    onRequestRefresh: () => void,
    isStationMenuOpen: boolean,
    availableStations: StationEntry[],
    selectedStation: number,
    isLoading: boolean
}

const KVBHeader = ({title, isStationMenuOpen, onHeaderClick, availableStations, selectedStation, onSelectStation, isLoading, onRequestRefresh}: KVBHeaderProps) => {
    return <Segment color="red" inverted attached="top" style={{borderTopLeftRadius: 0, borderTopRightRadius: 0}}>
        <Grid columns={2}>
            <Grid.Column>
                    <Header as="div" size="tiny" style={{margin: 0, color: "#eee"}}>Nächste Abfahrten</Header>
                    <Dropdown 
                        open={isStationMenuOpen}
                        text={title}
                        style={{fontSize: "1.5em", color: "#eee", fontWeight: "bold"}}
                        onClick={onHeaderClick}
                        value={selectedStation}
                        inline
                    >
                        <Dropdown.Menu>
                            <Dropdown.Item icon="edit" text="Haltestellen bearbeiten"/>
                            <Dropdown.Divider/>
                            {availableStations.map(station => {
                                return <Dropdown.Item key={station.code} onClick={() => onSelectStation(station.code)} text={station.name} active={station.code === selectedStation} />
                            })}
                        </Dropdown.Menu>
                    </Dropdown>
            </Grid.Column>
            <Grid.Column verticalAlign="middle" textAlign="right">
                {isLoading ? <Icon name="refresh" loading={isLoading} disabled /> : <div/>}
                <Dropdown icon={<Icon size="big" name="setting" />} pointing="top right">
                    <Dropdown.Menu>
                        <Dropdown.Item text="Einstellungen" icon="setting"/>
                        <Dropdown.Item text="Über" icon="info" />
                    </Dropdown.Menu>
                </Dropdown>
            </Grid.Column>
        </Grid>
        
        <div style={{clear: "both", marginBottom: "-1em"}} />
    </Segment>
}

interface TimetableProps {
    data: Maybe<Timetable>,
    isStationMenuOpen: boolean,
    toggleDepartureStationMenu: () => void,
    hideDepartureStationMenu: () => void,
    switchStation: (code: number) => void,
    requestRefresh: () => void,
    availableStations: StationEntry[],
    selectedStation: number,
    isLoading: boolean,
}

const Timetable = 
connect(
    (store: AppState) => {
        return {
            isStationMenuOpen: store.ui.departureMenuVisible,
            availableStations: store.timetable.availableStations,
            selectedStation: store.timetable.selectedStation,
            isLoading: store.timetable.updating
        };
    },
    (dispatch) => {
        return {
            toggleDepartureStationMenu: () => { dispatch(uiDepartureStationToggleMenuAction()) },
            hideDepartureStationMenu: () => { dispatch(uiDepartureStationHideMenuAction()) },
            switchStation: (code: number) => { dispatch(timetableSwitchStationAction(code)) },
            requestRefresh: () => { dispatch(timetableRefreshRequestedAction()) }
        };
    }
)(({
    data,
    isStationMenuOpen,
    toggleDepartureStationMenu,
    availableStations,
    selectedStation,
    switchStation,
    hideDepartureStationMenu,
    isLoading,
    requestRefresh
    }: TimetableProps) => {
        if(Maybe.isNothing(data))
            return <div>empty</div>
        
        return <div>    
            <KVBHeader 
                title={data.valueOrThrow().stationName}
                isStationMenuOpen={isStationMenuOpen} 
                onHeaderClick={() => {
                    if(!isLoading)
                        toggleDepartureStationMenu()
                }}
                availableStations={availableStations}
                selectedStation={selectedStation}
                isLoading={isLoading}
                onSelectStation={code => {
                    switchStation(code);
                    setTimeout(hideDepartureStationMenu, 0);
                }}
                onRequestRefresh={() => {
                    requestRefresh();
                }} />
            <Table color="red" attached size="large" unstackable>
                <TableBody>
                {data.map(data => data.departures.map((row, i) => 
                    <TimetableRow row={row} key={i} />
                )).valueOr([<TableRow key={0}><TableCell>Keine Abfahrten</TableCell></TableRow>])}
                </TableBody>
            </Table>
        </div>
});

export default Timetable;