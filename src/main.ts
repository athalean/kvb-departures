import * as React from 'react';
import * as ReactDOM from 'react-dom';

import { Root } from './components/app';

function mountApp(mountPoint: HTMLElement, station: number) {
    ReactDOM.render(React.createElement(Root, {initialStation: station}), mountPoint)
}

(window as any).mountApp = mountApp;