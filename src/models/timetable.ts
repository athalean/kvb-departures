

export interface TimetableDeparture {
    line: string;
    destination: string;
    departure: number | "now";
}

export interface Timetable {
    stationID: number,
    stationName: string;
    departures: TimetableDeparture[];
}

export interface TimetableState {
    lastUpdate: Date,
    data: Timetable
}