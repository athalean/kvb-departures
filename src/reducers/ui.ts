import { store } from '../components/app';
import { UIAction } from '../actions/ui'

export interface UIState {
    departureMenuVisible: boolean;
}

const defaultUIState: UIState = {
    departureMenuVisible: false
};

export default function uiReducer(state: UIState = defaultUIState, action: UIAction): UIState {

    switch(action.type) {
        case "ui-departure-station-toggle-menu":
            return {...state, departureMenuVisible: !state.departureMenuVisible};
        case "ui-departure-station-hide-menu":
            return {...state, departureMenuVisible: false}
    }

    return state;
}