import { Timetable } from '../models/timetable';
import { TimetableAction, timetableUpdateSucceededAction, timetableUpdateFailedAction, timetableRefreshRequestedAction } from '../actions/timetable';
import { fetchDepartures } from '../kvb/timetable'
import { Maybe } from 'tsmonad';
import { store } from '../components/app';

export interface StationEntry {
    code: number,
    name: string
}

export interface TimetableState {
    updating: boolean;
    dataFreshness: "fresh" | "outdated";
    error: Maybe<string>;
    data: Maybe<Timetable>;
    availableStations: StationEntry[];
    selectedStation: number;
}

const defaultTimetableState: TimetableState = {
    updating: false,
    dataFreshness: "outdated",
    error: Maybe.nothing(),
    data: Maybe.nothing(),
    availableStations: [
        {
            code: 2,
            name: "Neumarkt"
        },
        {
            code: 36,
            name: "Hansaring"
        },
        {
            code: 8,
            name: "Dom/Hbf"
        },
        {
            code: 30,
            name: "Friesenplatz"
        },
        {
            code: 362,
            name: "Wilhelm-Sollmann-Str."
        }
    ],
    selectedStation: 2
}

export default function(state: TimetableState = defaultTimetableState, action: TimetableAction): TimetableState {
    switch(action.type) {
        case "timetable-refresh-requested":
            fetchDepartures(state.selectedStation)
                .then(data => {
                    store.dispatch(
                        timetableUpdateSucceededAction(data)
                    );
                })
                .catch(e => {
                    store.dispatch(
                        timetableUpdateFailedAction(e.toString())
                    );
                });
            return {
                ...state,
                updating: true
            };
        case "timetable-update-succeeded":
            return {
                ...state,
                updating: false,
                dataFreshness: "fresh",
                data: Maybe.just(action.data),
                error: Maybe.nothing()
            }
        case "timetable-update-failed":
            return {
                ...state,
                updating: false,
                dataFreshness: "outdated"
            }
        case "timetable-switch-station":
            setTimeout(() => {
                store.dispatch(timetableRefreshRequestedAction())
            }, 0);

            return {
                ...state,
                selectedStation: action.newStation
            }
        
    }
    return state;
}
