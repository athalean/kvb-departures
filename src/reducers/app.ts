import { AppAction } from '../actions/app';
import { combineReducers } from 'redux';

import timetableReducer, { TimetableState } from './timetable';
import uiReducer, { UIState } from './ui';
export { TimetableState } from './timetable';

export interface AppState {
    timetable: TimetableState,
    ui: UIState
}

export default combineReducers({
    timetable: timetableReducer,
    ui: uiReducer
})